import groovy.transform.CompileStatic
import groovy.transform.ToString

@CompileStatic
@ToString(includeNames = true)
class FileData {
    String fullPath
    String filename
    String fileExtension
    long fileSize
    String destinationFolder
    String destinationFilename
    Date originalDate
    boolean organized
    boolean duplicate

    @Override
    boolean equals(Object other) {
        return (this.is(other) || (this.class.is(other?.class) && fullPath == (other as FileData).fullPath))
    }

    @Override
    int hashCode() {
        return Objects.hashCode(fullPath)
    }
}